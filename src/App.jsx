import React from "react";

import TodoForm from "./components/todo-form";
import TodoList from "./components/todo-list";

import './App.scss';

function App() {
  return (
    <div className="App">
      <h1>TODO APP</h1>
      <TodoForm />
      <TodoList />
    </div>
  );
}

export default App;
