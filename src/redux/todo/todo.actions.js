import * as todoActions from "./todo.action.types";

export const newTodo = (todo) => ({
    type: todoActions.TODO_ADD,
    payload: todo,
});

export const editTodo = (todos) => ({
    type: todoActions.TODO_EDIT,
    payload: todos,
});

export const deleteTodo = (todos) => ({
    type: todoActions.TODO_DELETE,
    payload: todos,
});
