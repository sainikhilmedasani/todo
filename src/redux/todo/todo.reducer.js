import * as todoActions from "./todo.action.types";

const initState = {
    todoList: []
}

const todoReducer = (state = initState, { type, payload }) => {
    switch(type) {
        case todoActions.TODO_ADD:
            return {
                ...state,
                todoList: [...state.todoList, payload]
            }
        case todoActions.TODO_EDIT: 
            return {
                ...state,
                todoList: [...payload]
            }
        case todoActions.TODO_DELETE:
            return {
                ...state,
                todoList: [...payload]
            }
        default:
            return {...state}
    }
}

export default todoReducer;