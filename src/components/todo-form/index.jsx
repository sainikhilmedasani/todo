import React, { useState } from "react";
import { connect } from "react-redux";
import { newTodo } from "../../redux/todo/todo.actions";

import "./todo-form.scss";

const TodoForm = ({ newTodo, value, onSave }) => {
    const [todoInput, setTodoInput] = useState(value ? value : "");
    const onInputChange = (event) => {
        setTodoInput(event.target.value)
    }

    const onFormSubmit = (event) => {
        event.preventDefault();
        if(todoInput) {
            if(value) {
                onSave(todoInput);
                setTodoInput("");
            }else {
                const todo = { todo: todoInput, done: false };
                newTodo(todo);
                setTodoInput("");
            }
        }
    }

    return (
        <div className="todo-form-container">
            <form noValidate onSubmit={onFormSubmit} className="todo-form">
                <div className="todo-form-input">
                    <input type="text" value={todoInput} onChange={onInputChange} />
                </div>
                <div className="todo-form-btn">
                    <button type="submit" className={`${value ? "save-btn" : ""}`}>{value ? "Save" : "Enter"}</button>
                </div>
            </form>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    newTodo: (todo) => dispatch(newTodo(todo))
})

export default connect(null, mapDispatchToProps)(TodoForm);
