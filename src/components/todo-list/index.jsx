import React from "react";
import { connect } from "react-redux";

import TodoItem from "./todoItem";
import { editTodo } from "../../redux/todo/todo.actions";

import "./todo-list.scss";

const TodoList = ({ todoList, editTodo }) => {
    const onCheckClick = (index) => {
        let list = [...todoList];
        list[index].done = !list[index].done;
        editTodo(list);
    }

    const onSaveEdit = (index, todoText) => {
        let list = [...todoList];
        list[index].todo = todoText;
        editTodo(list);
    }

    const onDeleteClick = (index) => {
        let list = [...todoList];
        list.splice(index, 1);
        editTodo(list);
    }

    return (
        <div className="todo-list">
            {todoList.map((todoItem, index) => (
                <TodoItem
                    todo={todoItem}
                    index={index}
                    onCheckClick={onCheckClick}
                    onSaveEdit={onSaveEdit}
                    onDeleteClick={onDeleteClick}
                />
            ))}
        </div>
    );
};

const mapStateToProps = (state) => ({
    todoList: state.todo.todoList,
});

const mapDispatchToProps = (dispatch) => ({
    editTodo: (todos) => dispatch(editTodo(todos))
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
