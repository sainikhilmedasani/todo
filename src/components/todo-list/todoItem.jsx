import React, { useState } from "react";

import Checkbox from "../checkbox";
import TodoForm from "../todo-form"

const TodoItem = ({ todo, index, onCheckClick, onSaveEdit, onDeleteClick }) => {
    const [isEdit, setIsEdit] = useState(false);
    return(
        <div className="todo-item">
            <div className="todo-item--checkbox"><Checkbox checked={todo.done} onClick={() => onCheckClick(index)} /></div>
            <div className="todo-item--text">
                {!isEdit ? 
                    (<p>{todo.todo}</p>)
                    :
                    (
                        <TodoForm
                            value={todo.todo}
                            onSave={(value) => {
                                onSaveEdit(index, value);
                                setIsEdit(false);
                            }}
                        />
                    )
                }
            </div>
            <div className="todo-item--action"><button className="todo-item--delete" onClick={() => onDeleteClick(index)}>Delete</button></div>
            <div className="todo-item--action"><button className="todo-item--edit" onClick={() => setIsEdit(true)}>Edit</button></div>
        </div>
    )
}

export default TodoItem;
