import React from "react";

import "./checkbox.scss";

const Checkbox = ({ checked, onClick }) => {
    return <div className={`checkbox ${checked ? "checked" : ""}`} onClick={() => onClick()} />
}

export default Checkbox;